include:
  - file: "/templates/renovate.gitlab-ci.yml"
    project: "renovate-bot/renovate-runner"
    ref: "v19.111.4"
  - file: "/.gitlab/ci/variables.yml"
    project: "ethima/gitlab-ci"
    ref: "v13.1.3"

# The `rules` of the `renovate` job are cleared as it should only be run when
# `extend`ed through `Manage Dependencies`. Its `stage` also needs to be
# updated to ensure it has a valid value
renovate:
  rules:
    - when: "never"
  stage: "Maintenance"

# Primarily a wrapper around the upstream `renovate` job, making sure that it
# fits in better with the rest of the @ethima ecosystem
Manage Dependencies:
  before_script:
    - |
      if [ -n "${DEPENDENCY_MANAGEMENT_AUTODISCOVERY}" ]; then
        export RENOVATE_AUTODISCOVER="true"
        export RENOVATE_AUTODISCOVER_FILTER="${DEPENDENCY_MANAGEMENT_AUTODISCOVERY}"
      fi
  extends: "renovate"
  image: "${CI_RENOVATE_IMAGE}"
  needs: []
  rules:
    - if: "$ENABLE_DEPENDENCY_MANAGEMENT == 'true'"
  stage: "Maintenance"
  variables:
    ETHIMA_DEPENDENCY_MANAGEMENT_GITHUB_TOKEN: "${ETHIMA_GITHUB_TOKEN}"
    ETHIMA_DEPENDENCY_MANAGEMENT_GITLAB_TOKEN: "${ETHIMA_GITLAB_TOKEN}"
    GITHUB_COM_TOKEN: "${ETHIMA_DEPENDENCY_MANAGEMENT_GITHUB_TOKEN}"
    RENOVATE_ASSIGNEES: "${DEPENDENCY_MANAGEMENT_ASSIGNEE}"
    RENOVATE_EXTENDS: "${DEPENDENCY_MANAGEMENT_CONFIGURATION}"
    RENOVATE_EXTRA_FLAGS: "${DEPENDENCY_MANAGEMENT_PROJECTS}"
    # Being enrolled into the dependency management process should require as
    # little effort as possible. Therefore onboarding is not required
    RENOVATE_ONBOARDING: "false"
    # The default dependency management configuration is intended to be
    # sufficient for most projects enrolled into Ethima processes. To reduce
    # the setup burden of the dependency management process, no configuration
    # should be required from projects enrolled into the process.
    #
    # Setting this to "optional" and not requiring onboarding ensures it is
    # still possible for a consuming project to specify a different
    # configuration, without forcing the creation of onboarding merge requests
    RENOVATE_REQUIRE_CONFIG: "optional"
    RENOVATE_TOKEN: "${ETHIMA_DEPENDENCY_MANAGEMENT_GITLAB_TOKEN}"

variables:
  DEPENDENCY_MANAGEMENT_ASSIGNEE:
    description: "The login of the user who should be assigned to dependency-related merge requests."
    value: "${GITLAB_USER_LOGIN}"
  DEPENDENCY_MANAGEMENT_AUTODISCOVERY:
    description: |
      A comma separated list of namespaces to automatically discover projects
      in. Supports minimatch glob-style or regular expression patterns. Leave
      empty to disable autodiscovery, or set to `**` to manage dependencies for
      all projects the process has access to. This setting overrides the
      projects configured through `DEPENDENCY_MANAGEMENT_PROJECTS`, a warning
      will be generated for configured projects that are not included in this
      list.
    value: ""
  DEPENDENCY_MANAGEMENT_CONFIGURATION:
    description: "The shareable configuration preset to use when managing dependencies."
    # Keep the default dependency management configuration which is used locked
    # to the same version as the active process
    #
    # BEGIN_VERSIONED_TEMPLATE
    # value: "gitlab>ethima/dependency-management#v__NEXT_SEMANTIC_RELEASE_VERSION__"
    # END_VERSIONED_TEMPLATE
    value: "gitlab>ethima/dependency-management#v3.0.0"
    # END_VERSIONED_TEMPLATE_REPLACEMENT
  DEPENDENCY_MANAGEMENT_PROJECTS:
    description: "A space separated list of project paths to manage dependencies for."
    value: "${CI_PROJECT_PATH}"
  ENABLE_DEPENDENCY_MANAGEMENT:
    description: "Whether to add the `Manage Dependencies` job to the pipeline."
    options:
      - "false"
      - "true"
    value: "false"
