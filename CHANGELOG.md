# Changelog

# [3.0.0](https://gitlab.com/ethima/dependency-management/compare/v2.0.0...v3.0.0) (2024-11-20)


* build(deps)!: update dependency renovate-bot/renovate-runner to v19 ([c3e1a50](https://gitlab.com/ethima/dependency-management/commit/c3e1a501e32e58c27fdfaaa97df64e58918c76fd))


### BREAKING CHANGES

* Update to Renovate v39. Clear any caches in case of
user filesystem conflicts. For full details on potentially necessary
configuration changes see
https://github.com/renovatebot/renovate/releases/tag/39.0.0.

# [2.0.0](https://gitlab.com/ethima/dependency-management/compare/v1.8.0...v2.0.0) (2024-11-01)


* feat(deps)!: update dependency renovate-bot/renovate-runner to v18 ([b494ffe](https://gitlab.com/ethima/dependency-management/commit/b494ffee20b79a7883c83cce500e74b58237e97f))


### BREAKING CHANGES

* Update to Renovate v38. For details on the respective
necessary configuration changes renovate-bot/renovate-runner!2863.

# [1.8.0](https://gitlab.com/ethima/dependency-management/compare/v1.7.0...v1.8.0) (2024-02-17)


### Features

* **deps:** update dependency renovate-bot/renovate-runner to v17.86.0 ([6adb7de](https://gitlab.com/ethima/dependency-management/commit/6adb7deafbd1945f4c51a70fce4a9e7dc90a41be))

# [1.7.0](https://gitlab.com/ethima/dependency-management/compare/v1.6.0...v1.7.0) (2023-12-26)


### Features

* support the `yaml` extension for GitLab CI definitions ([a53ddc5](https://gitlab.com/ethima/dependency-management/commit/a53ddc5decc107dc58ebbf45b017bf9f39ad3ed5))

# [1.6.0](https://gitlab.com/ethima/dependency-management/compare/v1.5.3...v1.6.0) (2023-12-26)


### Features

* support managing dependencies in variables for GitHub Actions workflows ([eaff166](https://gitlab.com/ethima/dependency-management/commit/eaff166c6ac1d6707d45a00ab437d4d7d8713c7f))

## [1.5.3](https://gitlab.com/ethima/dependency-management/compare/v1.5.2...v1.5.3) (2023-12-26)


### Bug Fixes

* allow snake-cased, multi-word dependency names in variable names ([d4661fc](https://gitlab.com/ethima/dependency-management/commit/d4661fcf9a6b2bbd1c45a131bfbbfcfeb86bd681)), closes [/gitlab.com/ethima/semantic-release/-/blob/8c26cc7061658510055360342bab67aa7fb1e7cc/.gitlab/ci/semantic-release.yml#L39-41](https://gitlab.com//gitlab.com/ethima/semantic-release/-/blob/8c26cc7061658510055360342bab67aa7fb1e7cc/.gitlab/ci/semantic-release.yml/issues/L39-41)

## [1.5.2](https://gitlab.com/ethima/dependency-management/compare/v1.5.1...v1.5.2) (2023-12-26)


### Bug Fixes

* use an allowed value for Renovate's `requireConfig` configuration ([6a51076](https://gitlab.com/ethima/dependency-management/commit/6a5107652ac7dd34fa4e80402c58cfd41a019e3a)), closes [/gitlab.com/ethima/dependency-management/-/blob/f3b11ab55eab5d457b133ccbe60a2d5827fabe6d/.gitlab/ci/manage-dependencies.yml#L39-41](https://gitlab.com//gitlab.com/ethima/dependency-management/-/blob/f3b11ab55eab5d457b133ccbe60a2d5827fabe6d/.gitlab/ci/manage-dependencies.yml/issues/L39-41)

## [1.5.1](https://gitlab.com/ethima/dependency-management/compare/v1.5.0...v1.5.1) (2023-10-29)


### Bug Fixes

* migrate to centalized Git actor definition ([22264ee](https://gitlab.com/ethima/dependency-management/commit/22264ee416ce13b45354f73d00c7738dbf00a225))

# [1.5.0](https://gitlab.com/ethima/dependency-management/compare/v1.4.0...v1.5.0) (2023-10-25)


### Features

* commit as "Ethima Bot" ([759ae89](https://gitlab.com/ethima/dependency-management/commit/759ae898b33517095d3f4c800972eeb189edae18))
* **deps:** update dependency ethima/gitlab-ci to v8 ([40b7667](https://gitlab.com/ethima/dependency-management/commit/40b76673c5bcda2abc658a2d34764c19429b9d00))

# [1.4.0](https://gitlab.com/ethima/dependency-management/compare/v1.3.1...v1.4.0) (2023-07-28)


### Features

* support "Ethima token" definitions for GitHub access ([d2c52cb](https://gitlab.com/ethima/dependency-management/commit/d2c52cbbdfb98e0169e34cfe7167d458495b0274))
* support "Ethima token" definitions for GitLab access ([98f086a](https://gitlab.com/ethima/dependency-management/commit/98f086acf14f43c3282a5bfe196596b3500247c5))

## [1.3.1](https://gitlab.com/ethima/dependency-management/compare/v1.3.0...v1.3.1) (2023-06-11)


### Bug Fixes

* **deps:** scope dependency management cache to dependency management ([b8df441](https://gitlab.com/ethima/dependency-management/commit/b8df4415c03dc6a5f90942cce728756450f7c908)), closes [/gitlab.com/ethima/julia/-/jobs/4452887830#L21](https://gitlab.com//gitlab.com/ethima/julia/-/jobs/4452887830/issues/L21)

# [1.3.0](https://gitlab.com/ethima/dependency-management/compare/v1.2.0...v1.3.0) (2023-05-05)


### Bug Fixes

* **deps:** update dependency ethima/gitlab-ci to v4.1.0 ([f6a7df6](https://gitlab.com/ethima/dependency-management/commit/f6a7df677c04fd3c0e07a97d6d51e6a19ddf7dba))


### Features

* migrate to `build` prefix for dependency updates ([21df7c8](https://gitlab.com/ethima/dependency-management/commit/21df7c8035f5d822e87ee0465b4b47142b6c4cb6)), closes [/docs.renovatebot.com/presets-default/#semanticcommittypeallarg0](https://gitlab.com//docs.renovatebot.com/presets-default//issues/semanticcommittypeallarg0)

# [1.2.0](https://gitlab.com/ethima/dependency-management/compare/v1.1.0...v1.2.0) (2023-04-30)


### Bug Fixes

* **preset:** stop assigning [@bauglir](https://gitlab.com/bauglir) to all PRs ([b1d728e](https://gitlab.com/ethima/dependency-management/commit/b1d728e3ee7d46f4b3670eaf6942a28c6bd322f1))


### Features

* **process:** add `DEPENDENCY_MANAGEMENT_ASSIGNEE` configuration variable ([5f86617](https://gitlab.com/ethima/dependency-management/commit/5f86617861ed3e44b61ec3cce46d87fc20e90a26))

# [1.1.0](https://gitlab.com/ethima/dependency-management/compare/v1.0.0...v1.1.0) (2023-04-30)


### Features

* **process:** add dependency management process ([9359b98](https://gitlab.com/ethima/dependency-management/commit/9359b98d11bb544e5ad677f45eff2e2bab507d6c))
* **process:** add job for managing dependencies ([81a3eae](https://gitlab.com/ethima/dependency-management/commit/81a3eae8bbc57293239e625ad85f8e39ec0b890b))

# 1.0.0 (2023-01-09)


### Bug Fixes

* correct `:automergeDigest` spelling ([fa7cea9](https://gitlab.com/ethima/dependency-management/commit/fa7cea9c8ee3539269e29bba2c86ec542c2f57d5))
* escape `$schema` keyword in onboarding config ([b73b5c5](https://gitlab.com/ethima/dependency-management/commit/b73b5c513345ebf785af3e8592d0d165876784c4))


### Features

* add basic configuration for Renovate ([377123d](https://gitlab.com/ethima/dependency-management/commit/377123d149192dd57078082ac723602a0a7d4906))
* automatically merge "digest" updates ([7d90cbc](https://gitlab.com/ethima/dependency-management/commit/7d90cbcb802ef4f47ec5161171982411987eb6a0))
* maintain NPM dependencies in GitLab CI process definitions ([85c68be](https://gitlab.com/ethima/dependency-management/commit/85c68be49842e36bbb31fed1e70d5c9770af93d2))
* manage GitLab CI `include`s in `.gitlab/ci/*.yml` ([39ecbcc](https://gitlab.com/ethima/dependency-management/commit/39ecbccc2c0966f8bb3abf05822fe08ffcecb8ab))
* manage GitLab CI dependencies in `.gitlab/ci` folders ([1df0fb0](https://gitlab.com/ethima/dependency-management/commit/1df0fb064f85fa75681b1d09fa92471de2610a86))
* pin digests of container images for immutability ([224c044](https://gitlab.com/ethima/dependency-management/commit/224c0444d014c0606b5a200767ba717c5791bcba))
* run Renovate on a schedule using the renovate-bot/renovate-runner> project ([a34cc1c](https://gitlab.com/ethima/dependency-management/commit/a34cc1cb7fb080cc08983564356aea4aab32b5f6))
