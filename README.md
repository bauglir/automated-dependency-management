# Dependency Management

A [reusable process](#process), implemented as a GitLab CI pipeline, and a
[shareable config preset](#shareable-config-preset) for managing dependencies
using [Renovate][renovate-url]. The process and preset are primarily targeted
towards projects using processes from the [Ethima
organization][ethima-organization-url] and particularly those processes
themselves, but can also be used outside of that context.

## Usage

Include the [process](#process) definition, which uses the [shareable config
preset](#shareable-config-preset) by default, into a project's
`/.gitlab-ci.yml` file using

<!-- BEGIN_VERSIONED_TEMPLATE

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/dependency-management"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"
```

-->

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/dependency-management"
    ref: "v3.0.0"
```

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

Alternatively, reference it as a [custom CI/CD configuration
file][gitlab-custom-cicd-configuration-file] in a project's settings.

The [`Manage Dependencies` job](#manage-dependencies) requires the
configuration of a `RENOVATE_TOKEN` environment variable, and optionally a
`GITHUB_COM_TOKEN` variable. See [the job's
documentation](#manage-dependencies) for details.

## Pipelines

### [Process](.gitlab/ci/process.yml)

The [`.gitlab/ci/process.yml`](.gitlab/ci/process.yml) file defines a complete
CI/CD pipeline to manage dependencies using [Renovate][renovate-url]. The
pipeline is derived from the renovate-bot/renovate-runner> project, but
adjusted to fit in with the rest of the [Ethima
ecosystem][ethima-organization-url].

Which [jobs](#jobs) are included in pipelines created through the process
depends on how the process was triggered. The following table outlines the
different scenarios:

| Job                                           | Scenario             |
| --------------------------------------------- | -------------------- |
| [`Manage Dependencies`](#manage-dependencies) | `Manual`, `Schedule` |

The process relies on [the composable workflow
scenarios][gitlab-ci-process-workflows] defined by the ethima/gitlab-ci>
project so that it can more easily compose with other processes in the
organization. See the associated documentation for information on how to base
other processes on this one and the available scenarios. The scenarios for this
process are defined under the `.Dependency Management Process` key.

## Jobs

For all jobs, see the `variables` sections in the corresponding files,
typically to the bottom, for an overview of the available configuration through
environment variables. These variables can be configured from the
`.gitlab-ci.yml` file, taking into account whether the variable is _global_ or
at the _job level_, or through GitLab's UI.

### [`Manage Dependencies`](.gitlab/ci/manage-dependencies.yml)

Runs [Renovate][renovate-url] using this project's [shareable config
preset](#shareable-config-preset). The job needs a few access tokens to be
configured as environment variables to be able to do its job.

| Environment Variable | Scopes                                 | Purpose                                                              |
| -------------------- | -------------------------------------- | -------------------------------------------------------------------- |
| `GITHUB_COM_TOKEN`   | Minimum scopes                         | Retrieving releases notes from GitHub.                               |
| `RENOVATE_TOKEN`     | `api`, `read_user`, `write_repository` | Creating accessing project repositories and managing merge requests. |

For which projects dependencies are managed is controlled through either the
`DEPENDENCY_MANAGEMENT_AUTODISCOVERY` or the `DEPENDENCY_MANAGEMENT_PROJECTS`
variable. When both are configured the latter has no effect and will typically
result in warnings being generated. Projects for which dependencies are managed
by this job do not typically require further
configuration/[onboarding][renovate-onboarding-url].

The `DEPENDENCY_MANAGEMENT_AUTODISCOVERY` environment variable may be set to a
comma-separated list of namespaces in which the bot should look for projects it
has access to. This list may include minimatch globs and regular expression
patterns and can be set to `**` to indicate that all projects the bot has
access to should be managed. For instance, this project is configured to manage
dependencies for all projects in the [Ethima
organization][ethima-organization-url] on a schedule by setting this variable
to `ethima/*`. This environment variable should be left empty, which is the
default, when using the `DEPENDENCY_MANAGEMENT_PROJECTS` variable.

The `DEPENDENCY_MANAGEMENT_PROJECTS` environment variable should be set to a
space separated list of projects to manage dependencies for. This defaults to
the project on which the job was triggered.

The `DEPENDENCY_MANAGEMENT_ASSIGNEE` environment variable may be set to the
username of the user who should be assigned to dependency-related merge
requests. Defaults to the username of the user triggering the pipeline in which
the job is included.

The `DEPENDENCY_MANAGEMENT_CONFIGURATION` environment variable may be used to
control the shareable config preset.

Inclusion of the job in a pipeline can be prevented by setting the
`ENABLE_DEPENDENCY_MANAGEMENT` environment variable to any value other than
`"true"`.

## [Shareable Config Preset](default.json)

A [shareable configuration preset][renovate-shareable-config-preset-url]
configured specifically to enable [Renovate][renovate-url] to manage projects
in the [Ethima organization][ethima-organization-url].

### Usage

<!-- BEGIN_VERSIONED_TEMPLATE

The preset is used by default by the [`Manage Dependencies`
job](#manage-dependencies). The preset can also be used separately. In that
case use it with Renovate's `extends` option set to
`gitlab>ethima/dependency-management#v__NEXT_SEMANTIC_RELEASE_VERSION__`.

-->

The preset is used by default by the [`Manage Dependencies`
job](#manage-dependencies). The preset can also be used separately. In that
case use it with Renovate's `extends` option set to
`gitlab>ethima/dependency-management#v3.0.0`.

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

### Features

- All functionality of Renovate's [`config:base`
  preset](https://docs.renovatebot.com/presets-config/#configbase).
- Enable [Renovate's "merge confidence" indicator](https://docs.renovatebot.com/merge-confidence/).
- Manage Docker and GitLab CI `include` dependencies in YAML files in a
  `/.gitlab/ci` folder.
- Manage NPM dependencies declared in variables in GitHub Actions workflows and
  Gitlab CI YAML files, e.g. `.github/workflows/*.ya?ml`, `/.gitlab-ci.ya?ml`,
  `/.gitlab/ci/*.ya?ml`. See
  [below](#management-of-npm-dependencies-in-variables-in-ci-definitions).
- Pin digests for Docker dependencies.

#### Management of NPM Dependencies in Variables in CI definitions

CI workflow definitions, e.g. GitHub Action workflows or GitLab CI files, may
depend on NPM packages requiring versioning, e.g. tools such as
[`prettier`](https://prettier.io), [`commitlint`](https://commitlint.js.org),
etc. A common approach is to define these dependencies directly within these
types of files in variables. Renovate is not capable of detecting these
dependencies by default. However, it can be extended to recognize these using
its ["custom manager support using regular
expressions"](https://docs.renovatebot.com/modules/manager/regex).

The preset searches for metadata added to these files and uses it to keep these
dependencies up-to-date. The following formats are supported (where
`Dependency` may be in titlecase or all lowercase):

- Explicitly declare the dependency's name in the metadata annotation

  ```
  # Dependency: name=@commitlint/cli
  COMMITLINT_VERSION: "17.2.0"
  ```

- Fully specify the dependency (including the version) as a variable's
  value:

  ```
  # Dependency:
  COMMITLINT: "@commitlint/cli@17.2.0"
  ```

- Suffix a variable's name with `_VERSION` where rest of the name maps onto
  the package's name when lowercased

  ```
  # Dependency:
  PRETTIER_VERSION: "2.7.1"
  ```

Note that annotations provided in the metadata take precedence over other
declarations! Hence the first example manages a dependency on
[`@commitlint/cli`](https://www.npmjs.com/package/@commitlint/cli), not
[`commitlint`](https://www.npmjs.com/package/commitlint) (although the latter
is an alias of the former).

Quotes around variable values are optional, but recommended.

[ethima-organization-url]: https://gitlab.com/ethima
[gitlab-ci-process-workflows]: https://gitlab.com/ethima/gitlab-ci/-/blob/51f0581e62d5a8cfa13d88ecaf946b01cf460a5a/README.md#workflows
[gitlab-custom-cicd-configuration-file]: https://docs.gitlab.com/15.11/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file
[renovate-onboarding-url]: https://docs.renovatebot.com/getting-started/installing-onboarding/#repository-onboarding
[renovate-shareable-config-preset-url]: https://docs.renovatebot.com/config-presets
[renovate-url]: https://renovatebot.com
